# Compilazione ed installazione di Apache httpd
## Sistema
- Ubuntu 19.04

## Installazione requisiti
```bash
sudo apt-get update
sudo apt-get install -y \
        build-essential \
        binutils \
        libpcre3-dev \
        libssl-dev \
        zlibc \
        zlib1g-dev \
        libexpat1-dev \
        ssl-cert \
        libxml2-dev \
        libyajl-dev \
```
## Download del sorgente
- Tutti i path che seguono sono per l'utente chiamato *cri*. Sostituite *cri* con il vostro nome utente.
- Il percorso in cui depositare i sorgenti e installare httpd è arbitrario, potete installarlo dove volete.
- È possbile aggiornare le versioni di questi sorgenti indicando link differenti. In questa guida sono usate le seguenti versioni:


|          | Versione |
| -------- | -------- |
| httpd    | 2.4.41   |
| apr      | 1.7.0    |
| apr-util | 1.6.1    |


```bash
cd 
mkdir apache_src
mkdir test_apache_1
cd apache_src

wget "http://it.apache.contactlab.it//httpd/httpd-2.4.41.tar.bz2"
wget "http://it.apache.contactlab.it//apr/apr-1.7.0.tar.bz2"
wget "http://it.apache.contactlab.it//apr/apr-util-1.6.1.tar.bz2"
```

## Estrazione
```bash
cd apache_src

HTTPD='httpd-2.4.41'
APR='apr-1.7.0'
APRUTIL='apr-util-1.6.1'

tar -xvjf ${HTTPD}.tar.bz2
tar -xvjf ${APR}.tar.bz2
tar -xvjf ${APRUTIL}.tar.bz2
```

## APR
```bash
cd
cd apache_src
mv $APR ${HTTPD}/srclib/apr
mv $APRUTIL ${HTTPD}/srclib/apr-util
```

## HTTPD
Seleziono un prefisso (una directory) in cui installare http. Nel mio caso ```/home/test_apache_1```

```bash
MYPREFIX='/home/test_apache_1'

cd /home/apache_src/${HTTPD}
./configure --prefix=$MYPREFIX --with-incuded-apr

make
make install
```

## Avvio e testing
```bash
sudo $MYPREFIX/bin/apachectl start

netstat -ntlp | grep 80

curl localhost
# o naviga su localhost dal browser
```