import java.net.*;
import java.io.*;

public class EchoClient 
{
 
 public static void main(String args[]){

String msg;

	try { Socket s =new Socket(args[0],7779);
// ATTENZIONE: la apertura di una socket puo
// generare eccezione ConnectException
// se il server non esiste o rifiuta il servizio
	
    System.out.println("socket aperta!");
   // recupera e sistema inputstream socket
	InputStream is = s.getInputStream();

		InputStreamReader ir =
 	 new InputStreamReader(is);
	BufferedReader r =
 				  new BufferedReader(ir);
   
   // recupera e sistema output stream socket
     OutputStream os = s.getOutputStream();

	 OutputStreamWriter or =
			 	new OutputStreamWriter(os);
     PrintWriter pw = new PrintWriter(os);

   // sistema input stream tastiera
     BufferedReader in = new BufferedReader(
			new InputStreamReader(System.in));

msg = "Pippo";

while(!msg.equals("stop"))
   {
   System.out.println("Inserisci una linea");
   // legge una stringa da input
   msg = in.readLine();
	// scrive la stringa sulla socket

    pw.println(msg);

    pw.flush();
	      
    System.out.println("Linea inviata al server");

	// legge una linea dalla socket
    String line = r.readLine();

    System.out.println("Linea ricevuta indietro, eccola");

    // la rimanda in output
    System.out.println(line);
    }

		s.close();
		} catch (UnknownHostException e){
		   System.err.println("Host unknown");
		   msg = "any";
		} catch (Exception e){
		   System.err.println(e);
		   msg = "any";
		}  }
}
