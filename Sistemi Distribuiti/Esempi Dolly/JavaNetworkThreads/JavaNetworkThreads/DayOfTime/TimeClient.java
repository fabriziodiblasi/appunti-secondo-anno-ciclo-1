import java.net.*;
import java.io.*;

public class TimeClient {
	
 public static void main(String args[]){
 
 Socket s = null;
 
System.out.println("Mi connetto a " + args[0]);
 
 try { s = new Socket(args[0],7777);

		InputStream is = s.getInputStream();

	InputStreamReader ir =
 	 new InputStreamReader(is);

	BufferedReader r =
 				  new BufferedReader(ir);

		String line = r.readLine();

		System.out.println(line);
		
		s.close();
		} catch (UnknownHostException e){
		   System.err.println("Computer Non Trovato");
		} catch (ConnectException e){
		   System.err.println("Connessione Non Riuscita");
		} catch (Exception e){
		   System.err.println(e);
		}
  }
}
