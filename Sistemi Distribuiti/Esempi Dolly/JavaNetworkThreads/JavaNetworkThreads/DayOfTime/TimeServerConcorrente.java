import java.net.*;
import java.io.*;
public class TimeServerConcorrente {

 public static void main(String args[]){
	ServerSocket ss = null;
	try {
	 ss = new ServerSocket(7777);
	 
	 while (true) {
		Socket clientSock = ss.accept();

        TimeThread tt = new TimeThread(clientSock);
        tt.start();
        /******************************  
		      **********************************/
   }
  } catch (UnknownHostException e){
		System.err.println("Host unknown");
	} catch (Exception e){
	   System.err.println(e);
	}
 }
}
