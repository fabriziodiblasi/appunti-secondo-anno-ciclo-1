import java.net.*;
import java.io.*;
public class TimeClientConta {

 public static void main(String args[]){
 Socket s = null;
 int conta;

 System.out.println("Invio Richiesta a " + args[0] + " " + args[1]);

 try { s = new Socket(args[0],7777);

 InputStream is = s.getInputStream();
 OutputStream os = s.getOutputStream();

 InputStreamReader ir =  new InputStreamReader(is);
 BufferedReader r = new BufferedReader(ir);

 DataOutputStream dos = new DataOutputStream(os);

 conta = Integer.parseInt(args[1]);

 dos.writeInt(conta);

 System.out.println("Inviato numero intero: " + conta);

 String line = r.readLine();

 System.out.println(line);
 s.close();

  } catch (UnknownHostException e){ System.err.println("Host unknown");
  } catch (Exception e){System.err.println(e);
		}
  }
}
