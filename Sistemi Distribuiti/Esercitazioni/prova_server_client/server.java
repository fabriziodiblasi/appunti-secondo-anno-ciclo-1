import java.net.*;
import java.io.*;

public class server {
    public static void main(String args[]){
        ServerSocket ss = null;
        try {
            ss = new ServerSocket(8080,5);
            while (true) {
                Socket clientSock = ss.accept();
                System.out.println("client connected");

                //---- parte aggiunta da me

                InputStream is = clientSock.getInputStream();
                InputStreamReader ir = new InputStreamReader(is);
                BufferedReader inr = new BufferedReader(ir);
                String line = inr.readLine();
                System.out.println("Ricevuto Client : ");
                System.out.println(line);

                //---- fine parte aggiuntiva

                OutputStream os = clientSock.getOutputStream();
                PrintStream outp = new PrintStream(os);
                outp.println(new java.util.Date());
                clientSock.close();
            }
        } catch (UnknownHostException e){
            System.err.println("Host unknown");
        } catch (Exception e){
            System.err.println(e);
        }
    }
}