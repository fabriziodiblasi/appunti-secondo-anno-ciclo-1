
public class Esempio5 {

  public static void main(String args[]){
	System.out.println("Main - inizio");

 MyThread5 t1,t2;
 t1 = new MyThread5("Primo Thread");
 t2 = new MyThread5("Secondo Thread");
 t1.start();
 t2.start();
	// NB: se � non-preemptive, va solo cip
	// se � preemptive, vanno entrambi
  }
}
