import java.net.*;
import java.io.*;

public class client {
    public static void main(String args[]){
        
        Socket s = null;
        try { 
            s = new Socket("localhost",8080);
            InputStream is = s.getInputStream();
            InputStreamReader ir = new InputStreamReader(is);
            BufferedReader inr = new BufferedReader(ir);
            OutputStream os = s.getOutputStream();
            PrintWriter pw = new PrintWriter(os);
            
            String msg = "Hello, I'm Donald Duck!";
            pw.println(msg);
            String line = inr.readLine();
            System.out.println("Ricevuto: ");
            System.out.println(line);
            s.close();
        } catch (UnknownHostException e){
            System.err.println("Host unknown");
        } catch (Exception e){ 
            System.err.println(e); 
        } 
    }
}